# Imagen MySQL para la asignatura Servicios Multimedia e Interactivos

Clona el repositorio y modifícalo según tus preferencias.

### Parámetros que pueden modificarse

 * Nombre de la base de datos que se crea automáticamente. Está definido en el fichero Dockerfile. Por defecto, se llama "company".
 * _Scripts_ de inicialización de la base de datos. Todos los _scripts_ SQL que se copien en el directorio "sql-scripts" se ejecutarán cuando se cree la imagen del contenedor. Por defecto, a modo de ejemplo, se incluyen dos _scripts_: uno crea una tabla "employees" y el otro inserta un nuevo registro en esta tabla.
 * La contraseña del usuario "root" de MySQL. La contraseña aparece en el fichero "docker-compose.yml" en el parámetro MYSQL_ROOT_PASSWORD. Por defecto, la contraseña es "example" sin comillas.
 * Puerto de escucha de MySQL. Por defecto, el servicio se mapea al puerto 33306 en lugar del puerto por defecto de MySQL (3306). En una máquina virtual con las extensiones de VirtualBox instaladas el puerto 3306 ya está en uso.


### Arrancar el contenedor

Para iniciar el contenedor:

```console
sudo docker-compose up -d
```

El contenedor volverá a arrancar automáticamente si se reinicia la máquina anfitriona.
